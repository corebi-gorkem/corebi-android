package com.corebi.api;


/**
 * <h1>CoreBI</h1>
 * This is the main CoreBI class that manages SDK instances.
 *
 * @see CoreBIClient CoreBIClient
 */
public class CoreBI {

    /**
     * Gets the default instance. This is the only method you should be calling on the
     * CoreBI class.
     *
     * @return the default instance
     */
    public static CoreBIClient getInstance() {
        return CoreBIClient.getInstance();
    }
}
