package com.corebi.api;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

class CoreBICallbacks implements Application.ActivityLifecycleCallbacks {

    public static final String TAG = "CoreBICallbacks";
    private static final String NULL_MESSAGE = "Need to initialize CoreBICallbacks with CoreBIClient instance";

    private CoreBIClient clientInstance = null;
    private static CoreBILog logger = CoreBILog.getLogger();

    public CoreBICallbacks(CoreBIClient clientInstance) {
        if (clientInstance == null) {
            logger.e(TAG, NULL_MESSAGE);
            return;
        }

        this.clientInstance = clientInstance;
        clientInstance.useForegroundTracking();
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {}

    @Override
    public void onActivityPaused(Activity activity) {
        if (clientInstance == null) {
            logger.e(TAG, NULL_MESSAGE);
            return;
        }

        clientInstance.onExitForeground(getCurrentTimeMillis());
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if (clientInstance == null) {
            logger.e(TAG, NULL_MESSAGE);
            return;
        }

        clientInstance.onEnterForeground(getCurrentTimeMillis());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outstate) {}

    @Override
    public void onActivityStarted(Activity activity) {}

    @Override
    public void onActivityStopped(Activity activity) {}

    protected long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }
}
